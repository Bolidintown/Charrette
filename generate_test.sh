#!/bin/bash
# https://github.com/Wandmalfarbe/pandoc-latex-template
# https://pandoc.org/MANUAL.html#variables-for-latex
#pandoc Informatique/Charduino/README.md -f markdown -o Informatique/test.pdf  --pdf-engine=xelatex \

emplacement=("Informatique/Charduino/" \
  "Electronique/" \
  "Equipement/manuel_montage/" \
  "Informatique/OpenSourceBikeShare/" )
titre=("Installation et utilisation du logiciel de l'assistance autonome" \
  "Assistance électrique de la Charrette" \
  "Montage des équipements vélo" \
  "Installation du système de mutualisation et partage de remorque" )
nom=("charduino-code" \
  "charduino-electronique" \
  "montage" \
  "opensourcebikeshare")

for i in {0..3}
do
  echo "${emplacement[$i]}"
  sed 's/\[TOC\]//g' "${emplacement[$i]}""README.md" > tmp.md
  pandoc tmp.md -f markdown -o "${emplacement[$i]}""/${nom[$i]}.pdf"  --pdf-engine=xelatex\
    --template template1.tex \
    -V "title: ${titre[$i]}" \
    -V "author: Charrette.bike" \
    -V 'date: 2022-11-05' \
    -V 'keywords: [Markdown, Example]' \
    -V 'titlepage' \
    -V 'toc: true' \
    -V 'toc-own-page: true' \
    -V 'toc-title: Table des matières' \
    -V 'lof: true' \
    -V 'lof-title: Table des figures' \
    -V 'titlepage-background: img/page1.png' \
    -V 'page-background: img/page.png' \
    -V 'page-background-opacity: 1' \
    -V 'titlepage-rule-color: 2a6166' \
    -V 'titlepage-rule-height: 0' \
    -V 'listings-no-page-break: true' \
    -V 'float-placement-figure:H'\
    -V 'listings-no-page-break:true'\
    -V 'colorlinks=true'
done


# -V 'numbersections:true'\

rm tmp.md
#  -V 'geometry:a4paper, landscape'
#-V 'footer-center: Plans sous licence CC-BY-SA \\ Remorque sous licence CERN-OHL-W V2' \
